#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

add-alias() {
	alias $1="$2"
	echo "alias $1='$2'" >> $DIR/aliases-custom.sh
}

alias date-iso="date +'%Y%m%dT%H%M'"

# Always start tmux in 256 color mode
alias tmux="TERM=screen-256color-bce tmux"

