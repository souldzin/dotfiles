call plug#begin('~/.vim/plugged')

Plug 'https://github.com/scrooloose/nerdtree.git'
Plug 'https://github.com/junegunn/limelight.vim.git'
Plug 'https://github.com/junegunn/goyo.vim'
Plug 'https://github.com/rafi/awesome-vim-colorschemes.git'
Plug 'https://github.com/reedes/vim-pencil.git'
Plug 'https://github.com/pangloss/vim-javascript'
Plug 'https://github.com/mxw/vim-jsx.git'
Plug 'https://github.com/vim-syntastic/syntastic'

" Initialize plugin system
call plug#end()

