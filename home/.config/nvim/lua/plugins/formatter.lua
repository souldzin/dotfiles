return {
	'stevearc/conform.nvim',
	opts = {},
	config = function(_, opts)
		require('conform').setup({
			default_format_opts = {
				lsp_format = "fallback",
			},
			format_on_save = {
				-- I recommend these options. See :help conform.format for details.
				lsp_format = "fallback",
				timeout_ms = 500,
			},

		})
	end
}
