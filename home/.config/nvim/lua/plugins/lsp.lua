return {
	"neovim/nvim-lspconfig",
	dependencies = {
		{
			"folke/neoconf.nvim",
			cmd = "Neoconf",
			opts = {},
		},
	},
	config = function(_, opts)
		local lspconfig = require('lspconfig')
		lspconfig.clangd.setup({
			cmd = {'clangd-12', '--background-index', '--clang-tidy', '--log=verbose'},
		})
	end
}

