# souldzin/dotfiles

Welcome to my dotfiles.  

Peace!

## How do I use this?

You can update your local "dotfiles" by running the command
`scripts/update.sh`. This will create symlinks from your home directory to
this one.

## I just ran `update.sh`... Now what?

You may also want to do the following:

### Update `.gitconfig`

This is where we set up git aliases. 

In your local `~/.gitconfig`, add the following:

```
[include]
  path = ~/.dot/.gitconfig
```

### Include `.bashrc`

This is where we set up environment variables.

In your local `~/.bashrc`, add the following:

```
source ~/.dot/.bashrc
```

