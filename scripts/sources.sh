#!/usr/bin/env bash
shopt -s dotglob

DIR="$(dirname "$0")"
DOTFILE_DIR="$(dirname $( cd $DIR ; pwd -P ))"

config_children=$(find ${DOTFILE_DIR}/home/.config -mindepth 1 -maxdepth 1)
home_children=$(find ${DOTFILE_DIR}/home -mindepth 1 -maxdepth 1 -not -name .config)

echo "$home_children"
echo "$config_children"

